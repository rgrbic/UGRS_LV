/* Template project for accelerometer readings
 *
 * R.Grbic
 */

#include "stm32f4xx.h"
#include "uart.h"
#include "LIS3DSH.h"
#include <stdio.h>

//global variables
char value[6];
volatile int16_t xval;
volatile int cnt = 0;
volatile int task1 = 0;		//read accelerometer
volatile int task2 = 0;		//send data to UART

volatile int16_t x_acc = 0;
volatile int16_t y_acc = 0;
volatile int16_t z_acc = 0;

//functions
void LED_TIM_Init(void)
{
	GPIO_InitTypeDef GPIO_InitLED;
	TIM_TimeBaseInitTypeDef TIM9InitStructure;

	// configure LEDs
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
	GPIO_InitLED.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_InitLED.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitLED.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(GPIOD, &GPIO_InitLED);

	//configure timer - interrupt every 1 ms (1000 Hz)
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM9, ENABLE);

	TIM9InitStructure.TIM_Prescaler = 9;
	TIM9InitStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM9InitStructure.TIM_Period = 16799;
	TIM9InitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInit(TIM9, &TIM9InitStructure);

	TIM_ITConfig(TIM9, TIM_IT_Update, ENABLE);
	TIM_Cmd(TIM9, ENABLE);

	NVIC_SetPriority(TIM1_BRK_TIM9_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0,0));
	NVIC_EnableIRQ(TIM1_BRK_TIM9_IRQn);
}


void TIM1_BRK_TIM9_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM9, TIM_IT_Update))
	{
		cnt++;					//update every 1 ms
		if(!(cnt%10))			//10 ms task
		{
			task1 = 1;
		}

		if(cnt == 100)			//100 ms task
		{
			task2 = 1;
			cnt = 0;
		}

		TIM_ClearITPendingBit(TIM9, TIM_IT_Update);
	}
}


int main(void)
{
	// configure LEDs, user button (external interrupt) and timer
	LED_TIM_Init();

	// configure UART1, pins PB6 i PB7
	USART1_Init();
	USART_PutString("STM32F4 discovery board\n");

	// configure LIS3DSH
	LIS3DSH_Init();

	if(LIS3DSH_Alive())
	{
		USART_PutString("LIS3DSH alive");
	}
	else
	{
		USART_PutString("No response from LIS3DSH");
	}
	USART_PutChar('\n');


    while(1)
    {
    	if(task1)
    	{
    		LIS3DSH_Read_XYZ(&x_acc, &y_acc, &z_acc);
    		task1 = 0;
    	}
    	if(task2)
    	{
    		GPIO_ToggleBits(GPIOD, GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15);
    		sprintf(value, "%i", x_acc);
    		USART_PutString(value);
    		USART_PutChar('\n');
    		task2 = 0;
    	}
    }
}




