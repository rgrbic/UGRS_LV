/*
 * LIS3DSH.c
 *
 *  Created on: Jun 1, 2018
 *      Author: rgrbic
 */

#include "LIS3DSH.h"


void LIS3DSH_Init(void)
{
	SPI_InitTypeDef SPI_InitStruct;
	GPIO_InitTypeDef GPIO_SPI_InitStruct;
	GPIO_InitTypeDef GPIO_CS_InitStruct;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOE , ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);

	// SCK, MISO, MOSI  - pins PA5, PA6, PA7
	GPIO_SPI_InitStruct.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_SPI_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_SPI_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_SPI_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_SPI_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA, &GPIO_SPI_InitStruct);

	// CS - pin PE3
	GPIO_CS_InitStruct.GPIO_Pin = GPIO_Pin_3;
	GPIO_CS_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_CS_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_CS_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_CS_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOE, &GPIO_CS_InitStruct);

	GPIO_SetBits(GPIOE, GPIO_Pin_3);

	GPIO_PinAFConfig(GPIOA, GPIO_PinSource5, GPIO_AF_SPI1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource6, GPIO_AF_SPI1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource7, GPIO_AF_SPI1);

	SPI_DeInit(SPI1);
	SPI_InitStruct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256;
	SPI_InitStruct.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStruct.SPI_Mode = SPI_Mode_Master;
	SPI_InitStruct.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStruct.SPI_NSS = SPI_NSS_Soft | SPI_NSSInternalSoft_Set;
	SPI_InitStruct.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_InitStruct.SPI_CPOL = SPI_CPOL_High;
	SPI_InitStruct.SPI_CPHA = SPI_CPHA_2Edge;

	SPI_Init(SPI1, &SPI_InitStruct);
	SPI_Cmd(SPI1, ENABLE);

	/* LIS3DSH CTRL_REG4:
		Output data rate = 400Hz
		continuous update
		X,Y,Z enabled
	 */
	LIS3DSH_Send(LIS3DSH_CTRL_REG4, 0x77);
}


uint8_t LIS3DSH_Alive(void)
{
	uint8_t receivedData = 0x00;
	receivedData = LIS3DSH_Receive(LIS3DSH_WHO_AM_I);

	if(receivedData == 0x3F)
		return 1;
	else
		return 0;
}


void LIS3DSH_Send(uint8_t adress, uint8_t data)
{
	GPIO_ResetBits(GPIOE, GPIO_Pin_3);

	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));
	SPI_I2S_SendData(SPI1, adress);
	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE));
	SPI_I2S_ReceiveData(SPI1);

	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));
	SPI_I2S_SendData(SPI1, data);
	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE));
	SPI_I2S_ReceiveData(SPI1);

	GPIO_SetBits(GPIOE, GPIO_Pin_3);
}


uint8_t LIS3DSH_Receive(uint8_t adress)
{
	GPIO_ResetBits(GPIOE, GPIO_Pin_3);

	adress = 0x80 | adress;

	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));
	SPI_I2S_SendData(SPI1, adress);
	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE));
	SPI_I2S_ReceiveData(SPI1); //Clear RXNE bit

	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));
	SPI_I2S_SendData(SPI1, 0x00); //Dummy byte to generate clock
	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE));

	GPIO_SetBits(GPIOE, GPIO_Pin_3);

	return SPI_I2S_ReceiveData(SPI1);
}


void LIS3DSH_Read_XYZ(volatile int16_t* xval, volatile int16_t* yval, volatile int16_t* zval)
{
	uint8_t tLSB = 0;			// LSB x-axis acceleration value
	uint8_t tMSB = 0;           // MSB x-axis acceleration value

	tLSB = LIS3DSH_Receive(LIS3DSH_OUT_X_L);
	tMSB = LIS3DSH_Receive(LIS3DSH_OUT_X_H);
	*xval =((tMSB<<8) | tLSB);

	tLSB = LIS3DSH_Receive(LIS3DSH_OUT_Y_L);
	tMSB = LIS3DSH_Receive(LIS3DSH_OUT_Y_H);
	*yval =((tMSB<<8) | tLSB);

	tLSB = LIS3DSH_Receive(LIS3DSH_OUT_Z_L);
	tMSB = LIS3DSH_Receive(LIS3DSH_OUT_Z_H);
	*zval =((tMSB<<8) | tLSB);
}


