/*
 * LIS3DSH.h
 *
 *  Created on: Jun 1, 2018
 *      Author: rgrbic
 */

#ifndef LIS3DSH_LIS3DSH_H_
#define LIS3DSH_LIS3DSH_H_

#include "stm32f4xx.h"

//LIS3DSH register adresses
#define LIS3DSH_OUT_T    		0x0C
#define LIS3DSH_INFO1			0x0D
#define LIS3DSH_INFO2			0x0E
#define LIS3DSH_WHO_AM_I   		0x0F
#define LIS3DSH_CTRL_REG1		0x21
#define LIS3DSH_CTRL_REG2		0x22
#define LIS3DSH_CTRL_REG3		0x23
#define LIS3DSH_CTRL_REG4   	0x20
#define LIS3DSH_CTRL_REG5   	0x24
#define LIS3DSH_CTRL_REG6   	0x25
#define LIS3DSH_STATUS			0x27
#define LIS3DSH_OUT_X_L     	0x28
#define LIS3DSH_OUT_X_H     	0x29
#define LIS3DSH_OUT_Y_L     	0x2A
#define LIS3DSH_OUT_Y_H     	0x2B
#define LIS3DSH_OUT_Z_L     	0x2C
#define LIS3DSH_OUT_Z_H			0x2D
#define LIS3DSH_FIFO_CTRL		0x2E


//function declarations
void LIS3DSH_Init(void);
uint8_t LIS3DSH_Alive(void);
void LIS3DSH_Send(uint8_t adress, uint8_t data);
uint8_t LIS3DSH_Receive(uint8_t adress);
void LIS3DSH_Read_XYZ(volatile int16_t* xval, volatile int16_t* yval, volatile int16_t* zval);


#endif /* LIS3DSH_LIS3DSH_H_ */
