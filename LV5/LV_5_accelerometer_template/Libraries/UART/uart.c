/*
 * uart.c
 *
 *  Created on: Jun 1, 2018
 *      Author: rgrbic
 */

#include "uart.h"

void USART1_Init(void)
{
	//UART
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);// Enable clock for GPIOB
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);// Enable clock for USART1

    // koristi se alternativna funkcija GPIO pinova PB6 i PB7
    GPIO_InitTypeDef GPIO_USART_InitStruct;
    GPIO_USART_InitStruct.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
    GPIO_USART_InitStruct.GPIO_Mode = GPIO_Mode_AF;
    GPIO_Init(GPIOB, &GPIO_USART_InitStruct);

	GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_USART1);// Connect PB6 to USART1_Tx
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource7, GPIO_AF_USART1);// Connect PB7 to USART1_Rx

    USART_InitTypeDef USART_InitStruct;
    USART_InitStruct.USART_BaudRate = 115200;							//brzina prijenosa 9600 bps
    USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None; //ne koristi se CTS i RTS
    USART_InitStruct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	//koristi se i prijemnik i predajnik
    USART_InitStruct.USART_Parity = USART_Parity_No;			//komunikacija bez pariteta
    USART_InitStruct.USART_StopBits = USART_StopBits_1;			//1 stop bit
    USART_InitStruct.USART_WordLength = USART_WordLength_8b;		//digitalna riječ koja se prenosi je veličine 8 bita
    USART_Init(USART1, &USART_InitStruct);							//zapisivanje UART1 postavki

    USART_Cmd(USART1, ENABLE);									//omogući rad UART1 periferije

    /*
    USART_ITConfig(USART1,USART_IT_RXNE,ENABLE);	 //omogući USART1 prekid uslijed primanja podataka

    //podesi priority grouping: 4 bita za preemptive priority, 0 bita za subpriority
    NVIC_SetPriorityGrouping(NVIC_PriorityGroup_4);

	//postavi prioritet USART1_IRQn prekida
    NVIC_SetPriority(USART1_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0,0));

    //omogući USART1_IRQn prekid
    NVIC_EnableIRQ(USART1_IRQn);
     */
}

void USART_PutChar(char c)
{
    // Wait until transmit data register is empty
    while (!USART_GetFlagStatus(USART1, USART_FLAG_TXE));
    // Send a char using USART1
    USART_SendData(USART1, c);
}

void USART_PutString(char *s)
{
    // Send a string
    while (*s)
    {
        USART_PutChar(*s++);
    }
}

uint16_t USART_GetChar(void)
{
    // Wait until data is received
    while (!USART_GetFlagStatus(USART1, USART_FLAG_RXNE));
    // Read received char
    return USART_ReceiveData(USART1);
}
