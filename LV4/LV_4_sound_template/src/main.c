#include "stm32f4xx.h"
#include "CS43L22.h"

volatile const uint16_t sineWave_128[128] = { 2048, 2145, 2242, 2339, 2435, 2530, 2624, 2717, 2808, 2897,
                                      	  	  2984, 3069, 3151, 3230, 3307, 3381, 3451, 3518, 3581, 3640,
											  3696, 3748, 3795, 3838, 3877, 3911, 3941, 3966, 3986, 4002,
											  4013, 4019, 4020, 4016, 4008, 3995, 3977, 3954, 3926, 3894,
											  3858, 3817, 3772, 3722, 3669, 3611, 3550, 3485, 3416, 3344,
											  3269, 3191, 3110, 3027, 2941, 2853, 2763, 2671, 2578, 2483,
											  2387, 2291, 2194, 2096, 1999, 1901, 1804, 1708, 1612, 1517,
											  1424, 1332, 1242, 1154, 1068, 985, 904, 826, 751, 679, 610,
											  545, 484, 426, 373, 323, 278, 237, 201, 169, 141, 118, 100,
											  87, 79, 75, 76, 82, 93, 109,  129, 154, 184, 218, 257, 300,
											  347, 399, 455, 514, 577, 644, 714, 788, 865, 944, 1026, 1111,
											  1198, 1287, 1378, 1471, 1565, 1660, 1756, 1853, 1950, 2047 };

volatile const uint16_t sineWave_24[24] = { 2048, 2593, 3098, 3526, 3843, 4028, 4065, 3953, 3700,
											3324, 2853, 2323, 1772, 1242, 771, 395, 142, 30, 67,
											252, 569, 997, 1502, 2047};

void TIM4_IRQHandler()
{
    if (TIM_GetITStatus(TIM4, TIM_IT_Update))
    {
    	//TODO:
    	//DODAJTE ODGOVARAJUĆI KOD

        TIM_ClearITPendingBit(TIM4, TIM_IT_Update);
    }
}

int main(void)
{
	/******* CS43L22 ***********/
	CS43L22_Init();					//analog passthrough način rada
	Passthrough_Volume_Low();		//postavljanje glasnoće


	/******* TIM4 ***********/
	//TODO:
	//KONFIGURIRAJTE I OMOGUĆITE RAD VREMENSKOG BROJAČA TIM4


	/******* D/A pretvornik ***********/
	//TODO:
	//KONFIGURIRAJTE PIN PA4
	//KONFIGURIRAJTE I OMOGUĆITE RAD D/A PRETVORNIKA 1


	/******* NVIC ***********/
	//TODO:
    //ODGOVARAJUĆA KONFIGURACIJA NVIC-A; TIM4 PREKID

    while(1)
    {
    	//šalji dummy podatke preko SPI kako bi CS43L22 bio aktivan
    	if (SPI_I2S_GetFlagStatus(CS43L22_I2S, SPI_I2S_FLAG_TXE))
    		SPI_I2S_SendData(CS43L22_I2S, 0x00);
    }
}

