import numpy as np
import matplotlib.pyplot as plt

NoPts = 24        #broj tocaka sinusne funkcije
DAC_bits = 12      #rezolucija D/A pretvornika
DAC_sat = 75       #dodatni offset od 0 i od 2^12

t = np.linspace(start=0, stop=2*np.pi, num=NoPts)
y =  np.sin(t)
y = y + 1;
y = y*((2**DAC_bits-1)-2*DAC_sat)/2 + DAC_sat;
y = np.round(y);

plt.plot(t,y)
plt.show()
file = open("testfile.txt","w");
file.write("volatile const uint16_t sine_wave[" + str(NoPts) + "]={");
print(range(0,len(y)))
for i in range(0,len(y)):
    file.write(str(int(y[i])));
    if i!=len(y)-1:
        file.write(",");


file.write("};");
file.close() 